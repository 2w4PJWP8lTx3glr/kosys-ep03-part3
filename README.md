こうしす！ #3.3
================

**「セキュリティに完璧を求めるのは間違っているだろうか」Part3**

情報セキュリティ普及啓発アニメ「こうしす！」シリーズ第3弾。

## 概要

これはオープンソースアニメーション作品「こうしす！#3.3」の制作用のリポジトリです。


## ライセンス
* [CC-BY 2.1 JP](http://creativecommons.org/licenses/by/2.1/jp/)
* [CC-BY 4.0](http://creativecommons.org/licenses/by/4.0/)
* [OPAP-UP 1.0](http://opap.jp/wiki/Licenses/OPAP-UP/1.0/jp)

一部異なるライセンスのファイルも含まれています。詳細はLICENSE.txtをご覧ください。

## クレジット
Copyright (C) OPAP-JP contributors. (https://opap.jp/contributors )

この作品は[CC-BY 4.0](http://creativecommons.org/licenses/by/4.0/)および[CC-BY 2.1 JP](http://creativecommons.org/licenses/by/2.1/jp/)のもとに提供されています。
この作品をご利用の際には、適切なクレジットの表示が必要です。クレジットや著作権についての詳細は [こうしす！クレジット](https://opap.jp/wiki/%E3%81%93%E3%81%86%E3%81%97%E3%81%99%EF%BC%81/%E3%82%AF%E3%83%AC%E3%82%B8%E3%83%83%E3%83%88) をご覧ください。

